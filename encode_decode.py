'''
Author: Tahsin Ahmed
Purpose: Encode-decode a message
Date: 12/08/2022
'''

import random
import string

#Converts val which is in decimal formot to binary, returns a string
def convertDeciToBin(val):

    if(val == 0):
        return ''
    else:
        return convertDeciToBin(val//2) + str(val % 2)

#Converts a binary number to decimal format
def convertBinToDeci(val):
    
    decimalFormat = 0
    currentBit = len(val) - 1
    for i in range(len(val)):
        decimalFormat += (int(val[i]) * (2 ** currentBit))
        currentBit -= 1
    return decimalFormat



#Takes a character and returns the binary represenatation of its ASCII value
def getBinaryRepOfCharacter(char):
    
    decimalValue = ord(char)
    binaryValue = convertDeciToBin(decimalValue)
    
    while(len(binaryValue) < 8):
        binaryValue = "0" + binaryValue
    
    return binaryValue


#Generates and returns a random string containing any available character
def generateRandomString(size):
    randomMessage = ''

    while len(randomMessage) != size:

        options = string.ascii_letters + string.digits + string.punctuation
        randomMessage += random.choice(options)
    return randomMessage

'''
A long string of 0's and 1's
@param: takes in a string which is a message that we want to encode
@returns: A string
'''
def generateOneTimePads(stringToEncode):
    
    pad = ''
    for each in stringToEncode:
        pad += getBinaryRepOfCharacter(each)
    
    return pad

#Get ASCII character of binary value
def mapBinToChar(val):
    start = 0
    end = 8
    newMessage = ''

    while start < len(val):
        decimalFormat = convertBinToDeci(val[start:end])
        start = end
        end += 8
        newMessage += chr(decimalFormat)
    return newMessage


#Takes the original message in binary form, and uses the pad to encrypt it. We do the XOR operation
def encrypt(original, key):
    result = ''
    for each in range(len(original)):
        if(original[each] != key[each] and (original[each] == '1' or key[each] == '1')):
            result += '1'
        else:
            result += '0'
    
    return result 

#Encode secret message using given mad
def encode(message, pad):

    key = generateOneTimePads(pad)
    padded_message = generateOneTimePads(message)

    encoded_binary = encrypt(padded_message, key)
    encoded_message = mapBinToChar(encoded_binary)

    return encoded_message

#Takes the encoded message in binary form, and uses the pad to decrypt it. We do the XOR operation
def decrypt(secret, key):
    result = ''
    for each in range(len(secret)):
        if(secret[each] != key[each] and (secret[each] == '1' or key[each] == '1')):
            result += '1'
        else:
            result += '0'
    
    return result 


#Decodes a message given the pad
def decode(encoded_message, pad):

    key = generateOneTimePads(pad)
    encodedMsgInBin = ''
    for each in encoded_message:
        encodedMsgInBin += getBinaryRepOfCharacter(each)
    
    decodedMsgInBin = decrypt(encodedMsgInBin, key)
    decodedMessage = mapBinToChar(decodedMsgInBin)
    return decodedMessage
    


if __name__ == "__main__":

    message = 'MATH 221 is a course in Discrete Mathematics'

    pad = generateRandomString(len(message))
    
    encoded_message = encode(message, pad)
    decoded_message = decode(encoded_message, pad)

    print("Encoded message is: ", encoded_message)
    print("Decoded message is: ", decoded_message)
