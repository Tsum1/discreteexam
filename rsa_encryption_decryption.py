import math
from prime_generator import generate_prime_number

def gcd(m,n): 
    if n==0: 
        return m
    else: 
        return gcd(n,m%n)

def generatePublicKey(phiOfN):

    for i in range(2, phiOfN):
        if gcd(phiOfN, i) == 1:
            return i


def generatePrivateKey(phiOfN, e):
    
    for i in range(phiOfN, 0, -1):
        if ( (e*i) % phiOfN) == 1:
           return i 


def encrypt(message, e, n):
    encrypted_message = ''
    encrypted_characters = []

    for each in message:
        encrypted = chr(int((math.pow(ord(each),e) % n)))
        encrypted_characters.append(encrypted)
    
    for each in encrypted_characters:
        encrypted_message += each

    return encrypted_message 

def decrypt(message, d, n):
    decrypted_message = ''
    decrypted_characters = []
    
    for each in message:
        decrypted = chr(int((ord(each) ** d % n)))
        decrypted_characters.append(decrypted)
    
    for each in decrypted_characters:
        decrypted_message += each

    return decrypted_message 


if __name__ == "__main__":

    secret_message = "This course discusses several different methods related to CRYPTOGRAPHY!"
    p = generate_prime_number(10)
    q = generate_prime_number(10)
    n = p * q
    phiOfN = (p-1) * (q-1)

    # 1 < e < phiOfN
    e = generatePublicKey(phiOfN)
    d = generatePrivateKey(phiOfN, e)

    #Encrypt the secret message using the public key we generated
    em = encrypt(secret_message, e, n)

    #Decrypt the encrypted message using the private key we have
    dm = decrypt(em, d, n)

    print("Encrypted message: ", em)
    print("Decrypted message: ", dm)


