import math
import time


def euclidGcd(a,b):
    if a > b:
        return euclidGcd(b, a)
    elif a == 0:
        return b
    else:
        return euclidGcd(a, b-a)


def gcd(a,b):
    num_iter = 0
    while a != 0:
        
        num_iter += 1
        #print("A is %d and b is %d" %(a, b))
        if a > b:
            temp = a
            a = b
            b = temp
        else:
            b = b - a
    return b, num_iter

def advancedGcd(a, b):

    r = 0

    #Find greatest common multiple of 2 raised to power r
    while(((a | b) & 1) == 0):
        
        a = a >> 1
        b = b >> 1
        r = r + 1

    iter = 0

    while a != 0:
        iter += 1
        #Swap the numbers 
        if a > b:
            temp = a
            a = b
            b = temp
        
        #If b is odd
        if ( a % 2 == 0) and (b % 2 == 1):
            a = a >> 1

        #If a is odd
        elif (a % 2 == 1) and (b % 2 == 0):
            b = b >> 1

        #If both a and b are odd
        elif (a % 2 == 1) and (b % 2 == 1):
            b = b - a

    print("Total iterations taken: %d" % iter)   
    return int((2 ** r) * b)

        

if __name__ == "__main__":

    

    
    
    firstHundredDigitInt = 9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
    secondHundredDigitInt = 1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
    #print("GCD is: %d" % advancedGcd(firstHundredDigitInt, secondHundredDigitInt))
    #print("Gcd is: %d" % advancedGcd(256, 16))
    
    
    #print("GCD is: %d" % euclidGcd(firstHundredDigitInt, secondHundredDigitInt))
    #g_cd, num_iter = gcd(firstHundredDigitInt, secondHundredDigitInt)
    #print("GCD is: %d" % (g_cd, num_iter))
    
    