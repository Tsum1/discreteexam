import time

def sieveOfEratosthenes(numberRange):
    
    numArray = []
    
    for num in range(numberRange+1):
        numArray.append(True)
    
    prime = 2
    
    while prime * prime <= numberRange:
        
        if prime:
            for num in range(prime * prime, numberRange+1, prime):
                numArray[num] = False
        prime += 1

    return numArray

def primeInfo(primeMarkedList):
    
    primeCount = 0
    currentNum = -1
    previousPrime = 0
    maxDif = 0
    prime1 = 0
    prime2 = 0
    
    for prime in primeMarkedList:
        currentNum += 1
        if prime:
            primeCount += 1
            if (currentNum - previousPrime >= maxDif):
                maxDif = currentNum -previousPrime
                prime1 = currentNum
                prime2 = previousPrime
            previousPrime = currentNum
    
    return (primeCount, maxDif, prime1, prime2)


def printPrime(primeMarkedList):
    for prime in range(2,len(primeMarkedList)):
        if primeMarkedList[prime]:
            print(prime, end= " ")
    print()
    

if __name__ == "__main__":
    
    primeNumbersInRange = 1000000

    time_dict = {}
    
    for num in range(0,1000001, 10000):
        start = time.time()
        sieveOfEratosthenes(num)
        end = time.time()
        time_pair = (start, end)
        time_dict[num] = time_pair

    print(time_dict)

    #startTime = time.time()
    #primeList = sieveOfEratosthenes(primeNumbersInRange)
    #endTime = time.time()

    #print("Total time taken to calculate primes in range %d is: %f" % (primeNumbersInRange, endTime - startTime))
    
    #primeData = primeInfo(primeList)
    
    #totalPrimesInRange = primeData[0]
    #maxDiff = primeData[1]
    #firstPrime = primeData[2]
    #secondPrime = primeData[3]
                
            
    #print("Total number of primes from 0 - %d are : %d" % (primeNumbersInRange, totalPrimesInRange))
    #print("The two primes in range %d with the largest difference are %d and %d and the difference is %d" % (primeNumbersInRange, firstPrime, secondPrime, maxDiff))
    
    
    #print("The first %d prime numbers are in range: 2 - %d" %(primeCount-2,primeNumbersInRange))
    #print("The first %d prime numbers are: " % (primeCount-2))
    
