from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp


def create_data_model():
    """Stores the data for the problem."""
    data = {}
    data['distance_matrix'] = [
        [0, 45, 57, 50, 42, 22, 36, 14, 22, 14, 41, 36, 28, 32, 36, 57, 50],
        [45, 0, 60, 22, 14, 36, 54, 32, 50, 58, 78, 61, 60, 71, 81, 82, 94],
        [57, 60, 0, 81, 71, 36, 22, 58, 36, 58, 50, 92, 85, 86, 73, 113, 81],
        [50, 22, 81, 0, 10, 51, 71, 36, 63, 64, 89, 51, 54, 67, 85, 70, 99],
        [42, 14, 71, 10, 0, 41, 61, 28, 54, 57, 81, 50, 51, 63, 78, 71, 92],
        [22, 36, 36, 51, 41, 0, 20, 22, 14, 30, 42, 57, 50, 54, 51, 78, 63],
        [36, 54, 22, 71, 61, 20, 0, 41, 14, 36, 32, 72, 64, 64, 51, 92, 60],
        [14, 32, 58, 36, 28, 22, 41, 0, 30, 28, 54, 36, 32, 40, 50, 58, 64],
        [22, 50, 36, 63, 54, 14, 14, 30, 0, 22, 28, 58, 50, 50, 40, 78, 51],
        [14, 58, 58, 64, 57, 30, 36, 28, 22, 0, 30, 41, 32, 28, 22, 58, 36],
        [41, 78, 50, 89, 81, 42, 32, 54, 28, 30, 0, 71, 61, 54, 28, 85, 32],
        [36, 61, 92, 51, 50, 57, 72, 36, 58, 41, 71, 0, 10, 22, 51, 22, 63],
        [28, 60, 85, 54, 51, 50, 64, 32, 50, 32, 61, 10, 0, 14, 41, 28, 54],
        [32, 71, 86, 67, 63, 54, 64, 40, 50, 28, 54, 22, 14, 0, 30, 32, 41],
        [36, 81, 73, 85, 78, 51, 51, 50, 40, 22, 28, 51, 41, 30, 0, 61, 14],
        [57, 82, 113, 70, 71, 78, 92, 58, 78, 58, 85, 22, 28, 32, 61, 0, 70],
        [50, 94, 81, 99, 92, 63, 60, 64, 51, 36, 32, 63, 54, 41, 14, 70, 0],
    ]
    data['num_vehicles'] = 6
    data['depot'] = 0
    return data


def print_solution(data, manager, routing, solution):
    """Prints solution on console."""
    print(f'Objective: {solution.ObjectiveValue()}')
    max_route_distance = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        route_distance = 0
        while not routing.IsEnd(index):
            plan_output += ' {} -> '.format(manager.IndexToNode(index))
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id)
        plan_output += '{}\n'.format(manager.IndexToNode(index))
        plan_output += 'Distance of the route: {}km\n'.format(route_distance)
        print(plan_output)
        max_route_distance = max(route_distance, max_route_distance)
    print('Maximum of the route distances: {}km'.format(max_route_distance))



def main():
    """Entry point of the program."""
    # Instantiate the data problem.
    data = create_data_model()

    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'], data['depot'])

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)


    # Create and register a transit callback.
    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # Define cost of each arc.
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Add Distance constraint.
    dimension_name = 'Distance'
    routing.AddDimension(
        transit_callback_index,
        0,  # no slack
        3000,  # vehicle maximum travel distance
        True,  # start cumul to zero
        dimension_name)
    distance_dimension = routing.GetDimensionOrDie(dimension_name)
    distance_dimension.SetGlobalSpanCostCoefficient(100)

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

    # Solve the problem.
    solution = routing.SolveWithParameters(search_parameters)

    # Print solution on console.
    if solution:
        print_solution(data, manager, routing, solution)
    else:
        print('No solution found !')

def calculate_distance(source, destination):
    import math

    sourceX = source[0]
    sourceY = source[1]

    destinationX = destination[0]
    destinationY = destination[1]

    xDistance = destinationX - sourceX
    yDistance = destinationY - sourceY

    return round(((xDistance ** 2) + (yDistance ** 2)) ** (0.5))

if __name__ == '__main__':
    
    locations = [(0, 0), 		# location 0 - the depot
            (-20, 40),    	# location 1
            (40, 40),     	# location 2
            (-40, 30),   	# location 3
            (-30, 30),	 	# location 4
            (10, 20),  		# location 5
            (30, 20),  		# location 6
            (-10, 10),  		# location 7
            (20, 10),  		# location 8
            (10, -10),  		# location 9
            (40, -10),  		# location 10
            (-30, -20),  	# location 11
            (-20, -20),  	# location 12
            (-10, -30 ),  	# location 13
            (20, -30),  		# location 14
            (-40, -40),    	# location 15
            (30, -40 )]  	# location 16
    
    distance = {}

    for i in range(len(locations)):

        distance[i] = []
        currentPos = locations[i]

        for each in locations:

            distance[i].append(calculate_distance(currentPos, each))
    
    
    main()
    

